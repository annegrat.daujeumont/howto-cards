#!/bin/sh

# change this with environment
DEFAULT_NOT_SOURCE_REGEX="/\\/assets\\//"
NOT_SOURCE_REGEX="${NOT_SOURCE_REGEX:-$DEFAULT_NOT_SOURCE_REGEX}"
TIMESTAMP_SUFFIX="${SUFFIX:-.timestamp}"
LOGFILE="${LOGFILE:-/dev/null}"

[ -z "$1" ] && echo "$0: no inputs specified?" >/dev/stderr

while [ -n "$1" ]
do
	echo "sourcing directory '$1' ..." >> "$LOGFILE"
	find "$1" -type f -name '*.md' | grep -v "$NOT_SOURCE_REGEX" | while read file ; do
		fn=`basename "$file"`
		dir=`dirname "$file"`
		tsfn="$fn.timestamp"
		(
			echo "making timestamp in '$dir' for file '$fn' ..." >> "$LOGFILE"
			cd "$dir"
			if [ -f "$tsfn" ]
			then echo "... but it already exists; skipping!" >> "$LOGFILE"
			else
				TIMESTAMP=`git log -n 1 --pretty=format:%cs -- "$fn"`
				mv "$fn" "$fn.temporary"
				head -n1 "$fn.temporary" > "$fn"
				echo "timestamp: \"$TIMESTAMP\"" >> "$fn"
				tail -n+2 "$fn.temporary" >> "$fn"
				rm -f "$fn.temporary"
			fi
		)
	done
	shift
done

---
layout: default
title: Cards
order: -1
---

{% include scripts.html %}

<style>
.noborderbox {
    border :0;
}
</style>

{% comment %}
	This code adds CSS that hides the index-boxes by default.
	It's in javascript, so that it doesn't effect the browsers with javascript disabled.
	How the correct boxes are shown then? box_hider.js shows them
{% endcomment %}
<script>
	var element = document.createElement('style');
	var content = document.createTextNode(".index-box {display: none;}");
	var head = document.getElementsByTagName('head');
	if (head.length > 0) {
		element.appendChild(content);
		head[0].appendChild(element);	
	}
</script>

<!-- index -->

<div class="index-box-container">

	<div class="index-box noborderbox" id="access-card">
		<h3>Access</h3>
		<ul>
			<li><a href="{{ 'external/access/harrenhal-access' | relative_url }}">HARRENHAL access</a></li>
			<li><a href="{{ 'external/access/lums-passwords' | relative_url }}">LUMS account</a></li>
			<li><a href="{{ 'external/access/passwords' | relative_url }}">Managing your passwords</a></li>
			<li><a href="{{ 'external/access/vpn-access' | relative_url }}">VPN connection</a></li>
			<li><a href="{{ 'external/access/vpn-cerbere-access' | relative_url }}">Connect to Cerbere via VPN</a></li>
			<li><a href="{{ 'external/access/vpn-mobile' | relative_url }}">VPN connection on your mobile phone</a></li>
			<li><a href="{{ 'external/access/wifiNetworkAccessGuests' | relative_url }}">WiFi network access for guests</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="backup-card">
		<h3>Backup</h3>
		<ul>
			<li><a href="{{ 'external/backup/computer' | relative_url }}">Staff Computer</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="contribute-card">
		<h3>Contribute</h3>
		<ul>
			<li><a href="{{ 'external/contribute/git-clients' | relative_url }}">Git clients</a></li>
			<li><a href="{{ 'external/contribute/install-git' | relative_url }}">Installation of Git</a></li>
			<li><a href="{{ 'external/contribute/markdown' | relative_url }}">Markdown</a></li>
			<li><a href="{{ 'external/contribute/mirror-fork' | relative_url }}">Mirror fork automatically</a></li>
			<li><a href="{{ 'external/contribute/review' | relative_url }}">Reviewing in Git</a></li>
			<li><a href="{{ 'external/contribute/ssh-key-generation' | relative_url }}">Key-based Authentication on GitLab</a></li>
			<li><a href="{{ 'external/contribute/supersede' | relative_url }}">Take over a stale merge request</a></li>
			<li><a href="{{ 'external/contribute/vscode' | relative_url }}">Contribute using Visual Studio Code</a></li>
			<li><a href="{{ 'external/contribute/web-ide' | relative_url }}">Contribute using Gitlab Web IDE</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="exchange-channels-card">
		<h3>Exchange channels</h3>
		<ul>
			<li><a href="{{ 'external/exchange-channels/asperaweb' | relative_url }}">AsperaWEB Quick Guide</a></li>
			<li><a href="{{ 'external/exchange-channels/atlas-hpc' | relative_url }}">Data transfer between Atlas and UL HPC Clusters</a></li>
			<li><a href="{{ 'external/exchange-channels/calendar' | relative_url }}">Sharing calendar in Microsoft Exchange</a></li>
			<li><a href="{{ 'external/exchange-channels/cryptomator' | relative_url }}">Cryptomator</a></li>
			<li><a href="{{ 'external/exchange-channels/lft' | relative_url }}">LCSB file transfer (LFT) Quick Guide</a></li>
			<li><a href="{{ 'external/exchange-channels/owncloud' | relative_url }}">Owncloud</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="general-card">
		<h3>General</h3>
		<ul>
			<li><a href="{{ 'external/general/usefulLinks' | relative_url }}">Useful links for living in Luxembourg</a></li>
			<li><a href="{{ 'external/general/getToLCSB' | relative_url }}">How to get to the Luxembourg Centre for Systems Biomedicine</a></li>
			<li><a href="{{ 'external/general/BelvalCampusMap' | relative_url }}">Belval Campus Map</a></li>
			<li><a href="{{ 'external/general/glossary' | relative_url }}">Glossary</a></li>
			<li><a href="{{ 'external/general/links' | relative_url }}">Important websites</a></li>
			<li><a href="{{ 'external/general/remote-working' | relative_url }}">Work remotely</a></li>
			<li><a href="{{ 'external/general/attend-webex' | relative_url }}">Attend the LCSB Team Meeting online (webex)</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="integrity-card">
		<h3>Integrity</h3>
		<ul>
			<li><a href="{{ 'external/integrity/checksum' | relative_url }}">Ensuring Integrity of Data Files with Checksums</a></li>
			<li><a href="{{ 'external/integrity/dmp' | relative_url }}">Data Management Plan</a></li>
			<li><a href="{{ 'external/integrity/encryption/disk' | relative_url }}">Encrypting the Startup Disk for Your Laptop/Desktop</a></li>
			<li><a href="{{ 'external/integrity/encryption/file' | relative_url }}">Encrypting Files and Folders</a></li>
			<li><a href="{{ 'external/integrity/naming' | relative_url }}">Naming files</a></li>
			<li><a href="{{ 'external/integrity/organization' | relative_url }}">Organization</a></li>
			<li><a href="{{ 'external/integrity/sanitisation' | relative_url }}">Sanitising Data Files</a></li>
			<li><a href="{{ 'external/integrity/spreadsheets' | relative_url }}">Working with spreadsheets</a></li>
			<li><a href="{{ 'external/integrity/transfer/owncloud-privatebin' | relative_url }}">Transfer of Human Data with OwnCloud</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-equipment-card">
		<h3>Lab: Equipment</h3>
		<ul>
			<li><a href="{{ 'external/lab-equipment/autoclaves' | relative_url }}">Autoclaves: utilization</a></li>
			<li><a href="{{ 'external/lab-equipment/balances' | relative_url }}">Balances: utilization and maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/biosafety-cabinets' | relative_url }}">Biosafety Cabinets: good practices</a></li>
			<li><a href="{{ 'external/lab-equipment/cold-traps' | relative_url }}">Cold traps: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/cryostorage' | relative_url }}">Cryostorage: utilization</a></li>
			<li><a href="{{ 'external/lab-equipment/dishwasher-utilization-and-maintenance' | relative_url }}">Dishwasher: utilization and maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/electric-car' | relative_url }}">Electric Car</a></li>
			<li><a href="{{ 'external/lab-equipment/freezers' | relative_url }}">-20°C Freezers: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/freezers-80' | relative_url }}">-80°C freezers: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/freezers-150' | relative_url }}">-150°C freezers: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/fridges' | relative_url }}">Fridges: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/hoods' | relative_url }}">Hoods: Laminar Flow - Fume Hood - Biosafety Cabinet</a></li>
			<li><a href="{{ 'external/lab-equipment/incubators' | relative_url }}">Incubators: maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/lightcycler' | relative_url }}">LightCycler: leave the virtual instrument mode</a></li>
			<li><a href="{{ 'external/lab-equipment/pH-meter' | relative_url }}">pH meter: utilization and maintenance</a></li>
			<li><a href="{{ 'external/lab-equipment/power-consumption' | relative_url }}">Power consumption of the equipment</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-good-practice-card">
		<h3>Lab: Good Practice</h3>
		<ul>
			<li><a href="{{ 'external/lab-good-practice/mycoplasma' | relative_url }}">Mycoplasma contamination check</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-hsa-card">
		<h3>Lab: Health & Safety, Access</h3>
		<ul>
			<li><a href="{{ 'external/lab-hsa/diphoterine' | relative_url }}">Diphoterine: utilization</a></li>
			<li><a href="{{ 'external/lab-hsa/handwashing' | relative_url }}">Handwashing</a></li>
			<li><a href="{{ 'external/lab-hsa/lab-coats' | relative_url }}">Lab coats</a></li>
			<li><a href="{{ 'external/lab-hsa/personal-alert-safety-system' | relative_url }}">Personal alert safety system (PASS)</a></li>
			<li><a href="{{ 'external/lab-hsa/pictograms' | relative_url }}">Pictograms</a></li>
			<li><a href="{{ 'external/lab-hsa/ppe' | relative_url }}">Personal Protective Equipment (PPE)</a></li>
			<li><a href="{{ 'external/lab-hsa/shipment' | relative_url }}">Shipment of biological or chemical samples with carrier</a></li>
			<li><a href="{{ 'external/lab-hsa/spill' | relative_url }}">Spill in a laboratory</a></li>
			<li><a href="{{ 'external/lab-hsa/spill-bsc' | relative_url }}">Spill in a Biosafety Cabinet</a></li>
			<li><a href="{{ 'external/lab-hsa/waste' | relative_url }}">Waste Management: Chemical and Biological waste</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-quarks-card">
		<h3>Lab: Quarks</h3>
		<ul>
			<li><a href="{{ 'external/lab-quarks/book-lab-equipment' | relative_url }}">Booking of a Lab Equipment</a></li>
			<li><a href="{{ 'external/lab-quarks/general' | relative_url }}">General information on Quarks</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="on-offboarding-card">
		<h3>On offboarding</h3>
		<ul>
			<li><a href="{{ 'external/on-offboarding/checklistBeforeArriving' | relative_url }}">Newcomer Checklist before Arriving in Luxembourg</a></li>
			<li><a href="{{ 'external/on-offboarding/checklistArrival' | relative_url }}">Checklist upon arrival in Luxembourg</a></li>
			<li><a href="{{ 'external/on-offboarding/onboarding' | relative_url }}">Onboarding new members at the LCSB</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="publication-card">
		<h3>Publication</h3>
		<ul>
			<li><a href="{{ 'external/publication/orcid' | relative_url }}">Obtain an ORCID</a></li>
			<li><a href="{{ 'external/publication/phdThesisTemplate' | relative_url }}">LaTeX template for a doctoral thesis at University of Luxembourg</a></li>
			<li><a href="{{ 'external/publication/10WaysImproveEnglish' | relative_url }}">10 ways to improve your English</a></li>
		</ul>
	</div>
	<div class="index-box noborderbox" id="publication-code-card">
		<h3>Publication: Publishing code and programs</h3>
		<ul>
			<li><a href="{{ 'external/publication-code/publish-repo' | relative_url }}">Publish a repository</a></li>
			<li><a href="{{ 'external/publication-code/add-gitignore' | relative_url }}">Add a .gitignore to your repository</a></li>
			<li><a href="{{ 'external/publication-code/publishInBiotools' | relative_url }}">Publishing a tool in *bio.tools*</a></li>
		</ul>
	</div>
</div><br><center><a href="{{ '/' | relative_url }}">go back</a></center><br><center><a href="{{ '/cards' | relative_url }}">Overview of all HowTo cards</a></center>
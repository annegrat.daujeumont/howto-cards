---
card_order: 450
layout: page
permalink: /external/access/vpn-mobile/
shortcut: access:vpn-mobile
redirect_from:
  - /cards/access:vpn-mobile
  - /external/cards/access:vpn-mobile
  - /access/vpn-mobile
  - /external/external/access/vpn-mobile/
---

# VPN connection on your mobile phone

## iPhone

- in the App store, download "Cisco Secure Client"

<div align="center">
<img src="img/img1.png" width="30%">
</div>

- click on *Connections*

<div align="center">
<img src="img/img2.png" width="30%">
</div>

- click on *Add VPN Connection...*

<div align="center">
<img src="img/img3.png" width="30%">
</div>

- in Server Address, write **vpn.uni.lu** and Save

<div align="center">
<img src="img/img4.png" width="30%">
</div>

- allow the *AnyConnect* to add VPN configurations

<div align="center">
<img src="img/img5.png" width="30%">
</div>

- enter your UL credentials

<div align="center">
<img src="img/img6.png" width="30%">
</div>

- to turn on/off the VPN, slide the button 'AnyConnect VPN'

<div align="center">
<img src="img/img7.png" width="30%">
</div>

---
layout: page
permalink: /external/integrity/transfer/owncloud-privatebin/
shortcut: integrity:transfer:owncloud-privatebin
redirect_from:
  - /cards/integrity/transfer:owncloud-privatebin
  - /external/integrity/transfer:owncloud-privatebin
  - /integrity/transfer/owncloud-privatebin
  - /external/external/integrity/transfer/owncloud-privatebin/
---

# Transfer of Human Data with OwnCloud

This How-to Card provides a step-by-step guide on how to transfer Human Data via LCSB Owncloud securely. Please note that this How-to Card is intended for the use case where data is < 4 GB and categorized as [sensitive]({{ '/?qms:LCSB-POL-BIC-01' | relative_url }}), in which encryption must be in place as well as secure exchange of encryption password with PrivateBin. If you wish to share Human Data **recurrently** with e.g. external collaborators, we recommend using Owncloud with Cryptomator for encryption of data on the cloud. For further information, please visit the dedicated card on [Cryptomator]({{ '/?exchange-channels:cryptomator' | relative_url}}).   

**Prerequisite:** LUMS account is needed and can be requested via the [Service Portal](https://service.uni.lu/sp?id=sc_cat_item&table=sc_cat_item&sys_id=c536257ddb336010ca53454039961936).

1. Use a zip software that supports AES256 encryption to zip your file(s) with a strong password. 
* For Windows computer you can use [7-zip](https://www.7-zip.org/download.html), which uses AES256 encryption by default. 
* For Mac you can use [Keka](https://www.keka.io/en/). To enable AES256 encryption, please go to `settings > Compression` and tick the **Use AES-256 encryption** checkbox like displayed below. <br>
<img src='img/keka-encryption.png' height='350px'> <br>
2. Use a password generator to generate a strong encryption password. 
* Use an online password generator like [dice ware](https://diceware.dmuth.org/), or a password manager like [BitWarden](https://bitwarden.com/) or [KeePass](https://keepass.info/) to generate a strong password.
3. Go to [LCSB PrivateBin](https://privatebin.lcsb.uni.lu/) and type/add the password in the Editor tab.   
4. Enable the feature "Burn after reading" by ticking the checkbox. This means that the link to the password can only be used **once** so it expires upon first access. 
<img src="img/encryptionpassword.png">
* You will be promted to enter your LUMS credentials once you click on "Send".
* You should be redirected to a page containing the password link. 
<img src="img/passwordLink.png">
5. Share the password link with your collaborator via your preferred communication channel.
6. The collaborator (recipient) **must** confirm that the password was successfully received before proceeding with the following steps. If collaborator reports an error, it indicates the password was compromised and data transfer is not secured anymore. In this case the zipped archive should be deleted and the process should started again. This is a crucial step in the data transfer!
7. Login to [OwnCloud](https://owncloud.lcsb.uni.lu/) with LUMS account.
* Upload the zipped archive to OwnCloud and make a share link with the collaborator. 
* See a full guide on how to use [OwnCloud]({{ '/?exchange-channels:owncloud' | relative_url }}).
<img src="img/owncloudshare.png">
8. Share the access link with your collaborator by typing in their email as shown on the image above. 
* The collaborator will automatically receive a link to the encrypted data on OwnCloud by email.  
9. The collaborator can now decrypt the data with the password received via Privatebin. 

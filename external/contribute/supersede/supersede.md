---
layout: page
permalink: /external/contribute/supersede/
shortcut: contribute:supersede
redirect_from:
  - /cards/contribute:supersede
  - /external/cards/contribute:supersede
  - /contribute/supersede
  - /external/external/contribute/supersede/
---

# Take over a stale merge request

In some cases, it might be useful to move the Merge Request of a branch opened by someone else to your own fork.

1. Open *Visual Studio Code*.
2. Select the ***develop*** branch and synchronize it.
3. In ***Terminal***, click on ***New Terminal***.

<div align="center">
<img src="img/img1.png" width="700">
</div>

**Steps 4 and 5 needs to be performed only the first time.**

4. If not done yet, configure the git mr command by following those [instructions](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#checkout-locally-by-adding-a-git-alias).

   4.1 Open *Git Bash*

    4.2 Type *vim ~/.gitconfig* - *Enter*

    4.3 Type *i*

    4.4 Type 

    [alias]

    mr = !sh -c 'git fetch $1 merge-requests/$2/head:mr-$1-$2 && git checkout mr-$1-$2' -

    <div align="center">
    <img src="img/img5.png" width="700">
    </div>

    4.5 Type *:wq*


5. In the *Terminal window*, type *git remote add upstream [paste the SSH Key]*.
You can copy the SSH key of the repository by clicking on Clone - copy on the repository page.

<div align="center">
<img src="img/img3.png" width="700" >
</div>
<div align="center">
<img src="img/img2.png" width="300" >
</div>

6. In the *Terminal window*, type *git mr upstream [number of the merge request]*.

    You are now on the branch of the merge request.

7. Push it to your fork by clicking on the cloud icone on the bottom left of your screen.

<div align="center">
<img src="img/img4.png"  width="100"  >
</div>

8. On GitLab, open a new Merge Request with the newly created branch.
    
    In description, write ***Supersedes ![number of the original MR]***.

9. In the original MR, write in the description ***Superseded by ![number of the newly created MR]*** and close the MR.
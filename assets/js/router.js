function UrlExists(url, cb) {
    jQuery.ajax({
        url:      url,
        dataType: 'text',
        type:     'GET',
        complete:  function(xhr){
            if(typeof cb === 'function')
            cb.apply(this, [xhr.status]);
        },
        headers: {
            'Cache-Control': 'no-cache, no-store, must-revalidate',
            'Pragma': 'no-cache',
            'Expires': '0'
        }
    });
}

function GetShortcutDestination() {
    var s = window.location.href;
    var pathArray = s.split('?');

    // Cut the query if it exists
    if (pathArray.length > 1) {
        return pathArray[1];
    } else {
        return '';
    }
}

function RedirectTo(newLocation) {
    setTimeout(function() {
        window.location.replace(newLocation);
    }, 1);
}

// Pick the shortcut link destination from URL, like: `category:subcategory:card-name`
var sub = GetShortcutDestination();


// If there's a shortcut to handle
if (sub.length > 0) {
    var externalCardURL = '/external/cards/' + sub;
    var internalCardURL = '/internal/cards/' + sub;

    UrlExists(externalCardURL, function(status){
        if (status == 200) { // The sub-card is external and accessible
            RedirectTo(externalCardURL);
        } else {
            UrlExists(internalCardURL, function(status){
                if (status == 200) { // The sub-card is internal and accessible
                    RedirectTo(internalCardURL);
                } else {
                    if (!sub.includes('search_query')) { // do not redirect when a search is performed
                        RedirectTo('/404.html'); // No external, no internal - probably no access
                    }
                }
            })
        }
    });
}
